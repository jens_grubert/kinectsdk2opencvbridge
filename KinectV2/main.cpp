#include "KinectColorStream.cpp"
#include <vector>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

Mat image = Mat(360, 640, CV_8UC4);

Mat rgbMat = Mat(1080, 1920, CV_8UC4);

Mat DepthMatRaw = Mat(424, 512, CV_16UC1);
Mat DepthMatDisp = Mat(424, 512, CV_16UC1);
Mat InfraredRawMat = Mat(424, 512, CV_16UC1);
Mat InfraredNormalisedMat = Mat(424, 512, CV_16UC1);


int MAX_DEPTH = 8000; // max sensing range of kinect is 8000 mm 
int MAX_USHORT_VALUE = 65535; 
// maxVal = 52685

ushort DEPTH_DISPLAY_SCALE = (ushort)(1.0 / MAX_DEPTH * MAX_USHORT_VALUE);

KinectColorStream ks;

int main(int argc, char** argv) {

	if (FAILED(ks.kinectSensorIntialise())) {
		std::cout << "No device found" << std::endl;
	}

	while (1) {
		ks.getDepthFrame(DepthMatRaw);		
		// stretch contrast of depth map so than we actually see something
		DepthMatDisp = DEPTH_DISPLAY_SCALE * DepthMatRaw;	
		//ks.getRGBFeedOptimised(image);
		ks.getRGBFeed(rgbMat);
		ks.getInfraredFrameRaw(InfraredRawMat);
		ks.getInfraredFrameNormalized(InfraredNormalisedMat);
		imshow("depth", DepthMatDisp);
		imshow("VideoStream", image);
		imshow("RGB", rgbMat);
		imshow("infraredRawStream", InfraredRawMat);
		imshow("infraredNormalisedStream", InfraredNormalisedMat);
		waitKey(1);
	}
}
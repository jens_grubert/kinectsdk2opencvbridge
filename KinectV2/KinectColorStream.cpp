
#include <SDKDDKVer.h>
#include <stdio.h>
#include <tchar.h>
#include <Windows.h>
#include <Kinect.h>
#include <opencv2/opencv.hpp>
#include <iostream>


using namespace std;
using namespace cv;

class KinectColorStream {

	#define InfraredSourceValueMaximum static_cast<float>(USHRT_MAX)
	#define InfraredSceneValueAverage 0.08f
	#define InfraredSceneStandardDeviations 3.0f
	#define InfraredOutputValueMinimum 0.01f 
	#define InfraredOutputValueMaximum 1.0f
	#define ROWS 424
	#define COLUMNS 512
	

	IKinectSensor* pSensor;
	HRESULT hResult = S_OK;

	//	RGB Feed Parameters
	IColorFrameSource* pColorSource;					// Color Source
	IColorFrameReader* pColorReader;					// Color Reader
	IFrameDescription* pColorFrameDescription;			// Color Frame Description
	IColorFrame* pColorFrame = nullptr;					// Color Frame

	IDepthFrameSource* pDepthSource;					// Depth Source
	IDepthFrameReader* pDepthReader;					// Depth Reader
	IFrameDescription* pDepthFrameDescription;			// Depth Frame Description
	IDepthFrame* pDepthFrame = nullptr;					// Depth Frame

	IInfraredFrameSource* pInfraredSource;				// Infrared Source
	IInfraredFrameReader* pInfraredReader;				// Infrared Reader
	IFrameDescription* pInfraredFrameDescription;		// Infrared Frame Description
	IInfraredFrame* pInfraredFrame = nullptr;			// Infrared Frame

	unsigned int DEPTH_FRAME_ROWS = 424;
	unsigned int DEPTH_FRAME_COLS =  512;


	unsigned int bufferSize = 1920 * 1080 * 4 * sizeof(unsigned char);
	unsigned int depthFrameSize = DEPTH_FRAME_COLS * DEPTH_FRAME_ROWS * 2 * sizeof(USHORT);
	Mat bufferMat = Mat(1080, 1920, CV_8UC4);
	Mat colorMat = Mat(480, 640, CV_8UC4);
	//Mat colorMat = Mat(480, 640, CV_8UC4);
	Mat currentFrame;

	UINT capacity = 0;
	ushort* buffer;

	ushort* infraredBuffer;
	UINT infraredBufferCapacity = 0;
	

	

public:
	template<class Interface>
	inline void SafeRelease(Interface *& pInterfaceToRelease)
	{
		if (pInterfaceToRelease != NULL) {
			pInterfaceToRelease->Release();
			pInterfaceToRelease = NULL;
		}
	}

	HRESULT kinectSensorIntialise() {

		HRESULT hResult = GetDefaultKinectSensor(&pSensor);
		if (FAILED(hResult)) {
			std::cerr << "Error : GetDefaultKinectSensor" << std::endl;
		}
		hResult = pSensor->Open();
		if (FAILED(hResult)) {
			std::cerr << "Error : IKinectSensor::Open()" << std::endl;
			return hResult;
		}
		return hResult;
	}

	void getRGBFeedOptimised(const cv::Mat& colorMat) {
		if (SUCCEEDED(pSensor->get_ColorFrameSource(&pColorSource))) {
			if (SUCCEEDED(pColorSource->OpenReader(&pColorReader))) {
				if (SUCCEEDED(pColorSource->get_FrameDescription(&pColorFrameDescription))) {
					pColorFrame = nullptr;
					hResult = pColorReader->AcquireLatestFrame(&pColorFrame);
					if (SUCCEEDED(hResult)) {
						unsigned int bytes = 0;
						int hei = 0;
						pColorFrameDescription->get_BytesPerPixel(&bytes);
						pColorFrameDescription->get_Height(&hei);

						//std::cout << bytes << std::endl;
						hResult = pColorFrame->CopyConvertedFrameDataToArray(bufferSize, reinterpret_cast<BYTE*>(bufferMat.data), ColorImageFormat::ColorImageFormat_Bgra);
						if (SUCCEEDED(hResult)) {
							resize(bufferMat, colorMat, colorMat.size(), 0, 0);
						}
					}
					SafeRelease(pColorFrame);
				}
			}
		}
	}

	void getRGBFeed(const cv::Mat& colorMat) {
		if (SUCCEEDED(pSensor->get_ColorFrameSource(&pColorSource))) {
			if (SUCCEEDED(pColorSource->OpenReader(&pColorReader))) {
				if (SUCCEEDED(pColorSource->get_FrameDescription(&pColorFrameDescription))) {
					pColorFrame = nullptr;
					hResult = pColorReader->AcquireLatestFrame(&pColorFrame);
					if (SUCCEEDED(hResult)) {
						unsigned int bytes = 0;
						int hei = 0;
						pColorFrameDescription->get_BytesPerPixel(&bytes);
						pColorFrameDescription->get_Height(&hei);

						//std::cout << bytes << std::endl;
						hResult = pColorFrame->CopyConvertedFrameDataToArray(bufferSize, reinterpret_cast<BYTE*>(colorMat.data), ColorImageFormat::ColorImageFormat_Bgra);
					
					}
					SafeRelease(pColorFrame);
				}
			}
		}
	}

	void getDepthFrame(cv::Mat& DepthMat)
	{
		if (SUCCEEDED(pSensor->get_DepthFrameSource(&pDepthSource))) {
			if (SUCCEEDED(pDepthSource->OpenReader(&pDepthReader))) {
				if (SUCCEEDED(pDepthSource->get_FrameDescription(&pDepthFrameDescription))) {
					pDepthFrame = nullptr;
					hResult = pDepthReader->AcquireLatestFrame(&pDepthFrame);
					if (SUCCEEDED(hResult)) 
					{
						pDepthFrame->AccessUnderlyingBuffer(&capacity, &buffer);
						
						//const UINT16* pBufferEnd = buffer + (512 * 424);

						int nRows = 424;
						int nCols = 512;
						const ushort* pBufferEnd = buffer + (nCols * nRows);
						
		
						int i = 0;
						int r = 0;
						int c = 0;
						for ( r = 0; r < nRows; ++r)
						{
							ushort* dat = DepthMat.ptr<ushort>(r);

							//p = I.ptr<uchar>(i);
							for ( c = 0; c < nCols; ++c)
							{
								
								ushort depth = buffer[i];
								
								dat[c] = depth;
								i++;
								//buffer++;
							}
						}
					}
					SafeRelease(pDepthFrame);					
				}
			}
		}
	}

	void getInfraredFrameRaw(cv::Mat& InfraredMat)
	{
		if (SUCCEEDED(pSensor->get_InfraredFrameSource(&pInfraredSource)))
		{
			if (SUCCEEDED(pInfraredSource->OpenReader(&pInfraredReader)))
			{
				if (SUCCEEDED(pInfraredSource->get_FrameDescription(&pInfraredFrameDescription)))
				{
					pInfraredFrame = nullptr;
					if (SUCCEEDED(pInfraredReader->AcquireLatestFrame(&pInfraredFrame)))
					{
						infraredBuffer = 0;
						pInfraredFrame->AccessUnderlyingBuffer(&infraredBufferCapacity, &infraredBuffer);

						const UINT16* infraredBufferEnd = infraredBuffer + (ROWS * COLUMNS);
						ushort* matPtr = InfraredMat.ptr<ushort>();

						while (infraredBuffer < infraredBufferEnd)
						{
							*matPtr = *infraredBuffer;
							++matPtr;
							++infraredBuffer;
						}
					}
					SafeRelease(pInfraredFrame);
				}
			}
		}
	}

	void getInfraredFrameNormalized(cv::Mat& InfraredMat)
	{
		if (SUCCEEDED(pSensor->get_InfraredFrameSource(&pInfraredSource)))
		{
			if (SUCCEEDED(pInfraredSource->OpenReader(&pInfraredReader)))
			{
				if (SUCCEEDED(pInfraredSource->get_FrameDescription(&pInfraredFrameDescription)))
				{
					pInfraredFrame = nullptr;
					if (SUCCEEDED(pInfraredReader->AcquireLatestFrame(&pInfraredFrame)))
					{
						infraredBuffer = 0;
						pInfraredFrame->AccessUnderlyingBuffer(&infraredBufferCapacity, &infraredBuffer);

						const ushort* infraredBufferEnd = infraredBuffer + (ROWS * COLUMNS);
						ushort* matPtr = InfraredMat.ptr<ushort>();

						while (infraredBuffer < infraredBufferEnd)
						{
							// normalize the incoming infrared data (ushort) to a float ranging from 
							// [InfraredOutputValueMinimum, InfraredOutputValueMaximum] by
							// 1. dividing the incoming value by the source maximum value
							float intensityRatio = static_cast<float>(*infraredBuffer) / InfraredSourceValueMaximum;

							// 2. dividing by the (average scene value * standard deviations)
							intensityRatio /= InfraredSceneValueAverage * InfraredSceneStandardDeviations;

							// 3. limiting the value to InfraredOutputValueMaximum
							intensityRatio = min(InfraredOutputValueMaximum, intensityRatio);

							// 4. limiting the lower value InfraredOutputValueMinimym
							intensityRatio = max(InfraredOutputValueMinimum, intensityRatio);
							
							// 5. converting the normalized value to a ushort and using the result
							*matPtr = static_cast<ushort>(intensityRatio * InfraredSourceValueMaximum);
							++matPtr;
							++infraredBuffer;
						}
					}
					SafeRelease(pInfraredFrame);
				}
			}
		}
	}

	void close() {
		SafeRelease(pColorSource);
		SafeRelease(pColorReader);
		SafeRelease(pColorFrameDescription);
		if (pSensor) {
			pSensor->Close();
		}
		SafeRelease(pSensor);
	}
};
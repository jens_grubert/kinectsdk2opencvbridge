#include "KinectV1Sensor.cpp"

int main()
{
	bool useIR = false; // only RGB or IR can be captured at a time
	// see https://social.msdn.microsoft.com/Forums/en-US/ca766c4c-00da-4ed6-b146-b8854989ebec/color-and-ir-simultaneously?forum=kinectsdk

	KinectV1Sensor kinectSensor = KinectV1Sensor(useIR);
	if (SUCCEEDED(kinectSensor.CreateFirstConnected()))
	{
		Mat color = Mat(480, 640, CV_8UC3);
		Mat depth = Mat(480, 640, CV_16U);
		Mat ir8bit = Mat(480, 640, CV_8UC1);

		while (1)
		{
			MsgWaitForMultipleObjects(kinectSensor.eventCount, kinectSensor.hEvents, FALSE, INFINITE, QS_ALLINPUT);
		
			if (useIR) {
				if (WAIT_OBJECT_0 == WaitForSingleObject(kinectSensor.m_hNextIRFrameEvent, 0))
				{
					kinectSensor.getIRFrame8bit(ir8bit);
				}
				imshow("IRStream", ir8bit);
			}
			else {
				if (WAIT_OBJECT_0 == WaitForSingleObject(kinectSensor.m_hNextColorFrameEvent, 0))
				{
					kinectSensor.getRGBFrame(color);
				}
				imshow("ColorStream", color);
			}
			
			// depth works simultaneously
			if (WAIT_OBJECT_0 == WaitForSingleObject(kinectSensor.m_hNextDepthFrameEvent, 0))
			{
				kinectSensor.getDepthFrame(depth);
			}			
			imshow("DepthStream", depth);
			
			waitKey(10);
		}

	}
}